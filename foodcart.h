/* Victoria Liu, Program 5, March 16th, 2023
 * This program is to allow users to enter a list
 * of their favorite food carts. Users can keep track 
 * of the food cart name, favorite food from the food cart
 * cost of the food, a description of the food from the food cart
 * and the rating from a scale of 1-10 of the food. The program 
 * will also let users choose if they want to display
 * all the items they have entered so far. This is the .h file
 * to the main.cpp */



#include <iostream>
#include <cctype>
#include <cstring>
#include <cstdlib>

using namespace std;

const int NAME=51;
const int DESCRIBE = 100; 

struct foodCart
{
char name[NAME];
char * favFood; 
int cost; 
char description[DESCRIBE];
int rating;
};

struct node
{
    foodCart data;
    node * next;
};

class list
{
	public: 
	void insert (foodCart newdata);
	void readCart();
	void welcome();
	void display_all(node* head);
	list();
	~list();

	private:
	node * head; 
};

