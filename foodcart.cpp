#include "foodcart.h"


list::list()
{
head = nullptr;
}

list :: ~list()
{
	while (head)
	{
		node * temp = head;
		head = head -> next; 
		delete temp; 
		temp = nullptr;
	}
}

void list::insert(foodCart newdata)
{

    node * temp = head;
    head = new node; 

    head -> data = foodCart newdata; 
    head -> next = temp; 
}

void list::readCart()
{
char cont; 
while (cont != 'n' || cont != 'N')
{
foodCart * ptr = new foodCart; 
ptr = new foodCart[10];
char temp [NAME];

cout << "Please enter the name of the foodcart. " << endl; 
cin.get(ptr->name, NAME, '\n');
cin.ignore(100, '\n'); 

cout << "Please enter the name of your favorite food. " << endl; 
cin.get (temp, NAME, '\n'); 
cin.ignore(100, '\n');
ptr -> favFood = new char [ strlen(temp) + 1];
strcpy(ptr -> favFood, temp);

cout << "Please enter the cost of the food. " << endl; 
cin >> ptr->cost; 
cin.ignore (100, '\n');

cout << "Please enter the description of the food. " << endl; 
cin.get (ptr->description, DESCRIBE, '\n'); 
cin.ignore(100, '\n'); 

cout << "Please enter the rating of the food cart from 1-10. " << endl; 
cin >> ptr-> rating; 
cin.ignore(100, '\n');

cout << "Do you want to continue? " << endl; 
cin >> cont;
cin.ignore(100, '\n');

}

}

void list::welcome()
{
cout << "This program's purpose is to allow users to keep track of foodcarts. " << endl; 
cout << "Users can enter the name of the foodcart, their favorite food, " << endl; 
cout << "the cost of the food, description of the food and they can also, " << endl; 
cout << "enter a rating for the foodcart from a scale from 1-10. " << endl; 
cout << "Users can also choose to display all the foodcarts entered. " << endl; 
}
